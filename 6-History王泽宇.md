# Appendix B - History
#附录B -历史

## Move to Github
## 迁移到Github

This essay has been created as a repository on Github so that it can be easily shared, updated and improved. It was copied from [http://samizdat.mines.edu/howto/HowToBeAProgrammer.htm](http://samizdat.mines.edu/howto/HowToBeAProgrammer.htm) by [Braydie Grove](https://github.com/braydie). It was moved to Github in January 2016.
这篇文章已被创建为一个库GitHub上，它可以很容易地共享、更新和改进。  这是来自[http://samizdat.mines.edu/howto/HowToBeAProgrammer.htm](http://samizdat.mines.edu/howto/HowToBeAProgrammer.htm) by [Braydie Grove](https://github.com/braydie). It was moved to Github in January 2016.

## Request for Feedback or Extension
## 反馈或扩展请求

Please send me any comments you may have on this essay. I consider all suggestions, many of which have already improved this essay.
请给我任何你可能会对这篇文章的评论。我会认真考虑所有的建议，从中改善了这篇文章。

I have placed this essay under the GNU Free Documentation License. This license is not specifically designed for essays. Essays are usually intended to be coherent and convincing arguments that are written from a single point of view in a single voice. I hope this essay is a short and pleasant read.
我已经把这篇文章GNU自由文档许可证。此许可证不是专为论文设计的。散文通常是一个连贯的，有说服力的论点，从一个单一的角度来看，在一个单一的声音。我希望这篇文章是一篇简短而令人愉快的阅读。

I also hope that it is instructive. Although not a textbook, it is broken into many small sections to which new sections can be freely added. If so inclined, you are encouraged to expand upon this essay as you see fit, subject to the provisions of the License.
我也希望它是有益的。虽然不是一本教科书，它被分解成许多小部分并且可以自由添加新的部分。如果是这样的话，

It may be arrogance to imagine that this document is worthy of extension; but hope springs eternal. I would be joyous if it were extended in the following ways:
想象这个文件值得去延伸有可能是自大的，但希望永远在播种。如果能在以下方面扩大我将倍感高兴：

The addition of a comprehensive reading list to each section,
另外一个全面的阅读清单，以每一节，

The addition of more and better sections,
越来越多的部分，

Translation into other languages, even if only on a subsection-by-subsection basis, and/or
翻译成其他语言，即使仅根据第款的分段，和/或

Criticism or commentary in-lined into the text.
对文本的评论或评论。

The ability to build into different formats, such as palm formats and better HTML.
打造成不同的格式，如HTML格式和手掌格式。

If you inform me of your work, I will consider it and may include it in subsequent versions that I produce, subject to the provisions of the License. You may of course produce your own versions of this document without my knowledge, as explained in the License.
如果你告诉我你的工作，我将考虑它和可能包含在我创造的随后的版本中，受许可证的规定。你可能会产生你自己的版本，本文件没有我的知识，例如在许可证。

Thank you.
谢谢你

Robert L. Read

## Original Version
## 原始版本

The original version of this document was begun by Robert L. Read in the year 2000 and first published electronically at Samizdat Press(http://Samizdat.mines.edu) in 2002. It is dedicated to the programmers of Hire.com.
该文件的原始版本是由Robert L. Read 2000年首次出版的刊物和电子媒体开始（http://samizdat.mines.edu）2002。它是专门为hire.com程序员。

After this article was mentioned on Slashdot in 2003, about 75 people sent me email with suggestions and errata. I appreciate them all. There was a lot of duplication, but the following people either made major suggestions or were the first to find a bug that I fixed: Morgan McGuire, David Mason, Tom Moertel, Ninja Programmer (145252) at Slashdot, Ben Vierck, Rob Hafernik, Mark Howe, Pieter Pareit, Brian Grayson, Zed A. Shaw, Steve Benz, Maksim Ioffe, Andrew Wu, David Jeschke, and Tom Corcoran.
2003年在Slashdot上发表这篇文章后,约75的人给我的建议和勘误的电子邮件。我很感激所有人。有很多重复,但下面的人作出了重大的建议或者是第一个发现我固定的一个错误：Morgan McGuire, David Mason, Tom Moertel, Ninja Programmer (145252) at Slashdot, Ben Vierck, Rob Hafernik, Mark Howe, Pieter Pareit, Brian Grayson, Zed A. Shaw, Steve Benz, Maksim Ioffe, Andrew Wu, David Jeschke, and Tom Corcoran.


Finally I would like to thank Christina Vallery, whose editing and proofreading greatly improved the second draft, and Wayne Allen, who encouraged me to initiate this.
最后，我要感谢 Christina Vallery，其编辑和校对大大提高第二稿的质量，还有Wayne Allen，他们鼓励我发起这个。

## Original Author's Bio
## 原始作者的Bio

Robert L. Read lives in Austin, Texas, with his wife and two children. He is currently a Principal Engineer at Hire.com, where he has worked for four years. Prior to that he founded 4R Technology, which made a scanner-based image analysis quality control tool for the paper industry.
Robert L. Read 住在奥斯汀,德克萨斯，和他的妻子和两个孩子。他目前是hire.com的首席工程师，他在那里工作了四年。在此之前，他创立了4R技术，使扫描图像分析的质量控制工具，用于造纸工业。

Rob received a PhD from the University of Texas at Austin in 1995 in Computer Science related to database theory. In 1987 he received a BA in Computer Science from Rice University. He has been a paid programmer since the age of 16.
Rob 获得得克萨斯大学奥斯汀分校博士学位，在1995的计算机科学相关的数据库理论。1987他在莱斯大学获得了计算机科学学士学位。他从16岁开始就一直是一个付费程序员。

Next [License](LICENSE)