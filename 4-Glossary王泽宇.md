# Glossary 王泽宇
# 术语表 王泽宇

This is a glossary of terms as used in this essay. These do not necessarily have a standardized meaning to other people. Eric S. Raymond has compiled a massive and informative glossary[HackerDict] that rather surprisingly can pleasurably be read cover-to-cover once you can appreciate a fraction of it.
这是这篇文章使用的术语表。这些不一定有一个标准化的意义。这些对一些人来说不一定有一个标准化的意义。埃里克·s·雷蒙德编撰了一个巨大的且内容丰富的词汇表(HackerDict),令人惊讶的是可以愉悦的从头到尾阅读一旦你欣赏到它的一部分。

**unk-unk**
**未知因素**
: Slang for unknown-unknown. Problems that cannot presently even be conceptualized that will steal time away from the project and wreck the schedule.
：未知因素的俚语。:未知的未知的俚语。问题,目前甚至无法概念化,抢走项目和破坏时间安排。

**boss** 
**老板**
: The person or entity that gives you tasks. In some cases this may be the public at large.
：给你任务的个人或实体。在某些情况下这可能是公众。


**printlining**
**打印行**
: The insertion of statements into a program on a strictly temporary basis that output information about the execution of the program for the purpose of debugging.
：语句插入到一个程序中，在一个程序中，在一个严格的临时基础上，输出信息，用于调试程序的执行。

**logging**
**记录**
: The practice of writing a program so that it can produce a configurable output log describing its execution.
：编写一个程序，以便它可以生成一个可配置的输出日志来描述它的执行。

**divide and conquer**
**分部解决**
: A technique of top-down design and, importantly, of debugging that is the subdivision of a problem or a mystery into progressively smaller problems or mysteries.
：

**vapour**
**蒸汽**
: Illusionary and often deceptive promises of software that is not yet for sale and, as often as not, will never materialize into anything solid.
：错觉的和经常迷惑的

**boss**
**老板**
: The person who sets your tasks. In some cases, the user is the boss.
：设定任务的人。在某些情况下，用户是老板。

**tribe**
**trib**
: The people with whom you share loyalty to a common goal.
：与同你分享的人共同忠诚于一个目标。

**low-hanging fruit**
**轻易实现的目标**
: Big improvements that cost little.
：成本小，改进大。

**Entrepreneur**
**主办者**
:The initiator of projects.
：项目的发起人。

**garbage**
**垃圾**
: Objects that are no longer needed that hold memory.
：不再需要记忆的对象。

**business**
**业务**
: A group of people organized for making money.
：为赚钱而组织起来的一群人。

**company**
**公司**
: A group of people organized for making money.
：为赚钱而组织起来的一群人。

**tribe**
**部落**
: A group of people you share cultural affinity and loyalty with.
：一群彼此分享文化亲和力和忠诚的人。

**scroll blindness**
**卷轴盲点**
: The effect of being unable to find information you need because it is buried in too much other, less interesting information.
：无法找到你需要的信息，因为它被埋在太多，不太有趣的信息里。

**wall-clock**
**网络挂钟**
: Actually time as measured by a clock on a wall, as opposed to CPU time.
：实际上，时间是由一个在墙上的时钟测量，而不是中央处理器时间测量。

**bottleneck**
**障碍物**
: The most important limitation in the performance of a system. A constriction that limits performance.
：系统性能最重要的限制。限制性能的收缩。

**master**
**大师**
: A unique piece of information from which all cached copies are derived that serves as the official definition of that data.
：一个独特的信息，从中的所有缓存的副本，作为该数据的官方定义。

**heap allocated**
**以堆形式分配**
: Memory can be said to be heap allocated whenever the mechanism for freeing it is complicated.
：内存可以说是堆分配的机制，用于释放它是复杂的。

**garbage**
**垃圾**
: Allocated memory that no longer has any useful meaning.
：分配的内存，不再有任何有用的含义。

**garbage collector**
**垃圾收集器**
: A system for recycling garbage.
: 回收垃圾的系统。

**memory leak**
**内存泄露**
: The unwanted collection of references to objects that prevents garbage collection (or a bug in the garbage collector or memory management system!) that causes the program to gradually increase its memory demands over time.
：不想要的引用的集合，以防止垃圾收集（或一个错误的垃圾收集器或内存管理系统）这会导致程序的内存需求随着时间的推移逐渐增加。

**Extreme Programming**
**极限编程**
: A style of programming emphasizing communication with the customer and automated testing.

**hitting the wall**
**撞墙**
: To run out of a specific resource causing performance to degrade sharply rather than gradually.
：运行某一特定资源导致的性能突然下降而不是逐渐下降。

**speculative programming**
**投机编程**
: Producing a feature before it is really known if that feature will be useful.
：制作一个功能之前，是真的知道这个功能将是有用的。

**information hiding**
**信息隐藏**
: A design principle that seeks to keep things independent and decoupled by using interfaces that expose as little information as possible.
：一种设计原则，旨在通过使用界面，尽可能少的信息，以保持事物的独立和解耦。

**object-oriented programming**
**面向对象程序设计**
: An programming style emphasizing the the management of state inside objects.
：一种强调对象状态管理的程序设计风格。

**communication languages**
**交际语言**
: A language designed primarily for standardization rather than execution.
：一种语言，主要用于标准化而不是执行。

**boxes and arrows**
**箱子和箭头**
: A loose, informal style of making diagrams consisting of boxes and arrows drawn between those boxes to show the relationships. This contrast with formal diagram methodologies, such as UML.
：一种松散的、非正式的制作图表的形式，包括在这些盒子之间绘制的方框和箭头来显示关系。与形式图方法的对比度，如UML。

**lingua franca**
**通用语**
: A language so popular as to be a de facto standard for its field, as French was for international diplomacy at one time.
：一种流行的操作系统语言的标准，例如法国在同一时间运用其进行国际外交。

**buy vs. build**
**购买与建设**
: An adjective describing a choice between spending money for software or writing it your self.
：描述一个用于软件或写它自己的选择之间的形容词。

**mere work**
**单纯的工作**
: Work that requires little creativity and entails little risk. Mere work can be estimated easily.
:工作，需要很少的创造力，并带来小风险。简单的工作可以很容易地估计。

**programming notation**
**编程符号**
: A synonym for programming language that emphasizes the mathematical nature of programming language and their relative simplicity compared to natural languages.
:编程语言的同义词,强调编程语言的数学性质和他们的相对简单而自然语言。

**strawman**
**稻草人**
: A document meant to be the starting point of a technical discussion. A strawman may lead to a stickman, tinman, woodman, ironman, etc.
：一个文档是技术讨论的起点。稻草人可能导致一个曲棍球手洋铁匠,樵夫,铁人,等等。

**white-paper**
**白皮书**
: An informative document that is often meant to explain or sell a product or idea to an audience different than the programmers of that product or idea.
：一个有用的文档,通常是为了向观众解释或出售的产品或想法不同的程序员的产品或想法。


Next [Bibliography/Websiteography](5-Bibliography.md)