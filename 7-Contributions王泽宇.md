# Contributions
# 贡献

This repository aims to be a community driven project, and your involvement will ultimately help improve the quality of this guide. 
这个库的目的是成为一个社区驱动的项目，您的参与将最终帮助提高本指南的质量。

## What can I do to contribute?
## 我能做些什么贡献？

There are a number of ways to contribute to "How to be a Programmer".
有许多方法可以促进“如何成为一名程序员”。

- Ideas for new sections
- 新章节的想法
- Improvements to existing sections
- 改进现有的部分
- Identifying typos or other issues in sections
- 识别部分拼写错误或其他问题
- Contributing additional links to resources for sections
- 为各部门提供额外的资源链接
- General suggestions for improving the project
- 改善项目的一般性建议

## Contributors
## 贡献

Github holds a list of all [contributors](https://github.com/braydie/HowToBeAProgrammer/graphs/contributors) to this project.
Github 拥有所有列表到这个项目

## Editorship and Move to GitHub
## 编辑和移动到GitHub

[Braydie Grove](https://www.github.com/braydie) has agreed to serve as editor-in-chief.
[Braydie Grove](https://www.github.com/braydie) 已同意担任总编辑。

Braydie transposed the original essay into MarkDown and created the repository.
转置最初的文章到MarkDown和创建存储库。
